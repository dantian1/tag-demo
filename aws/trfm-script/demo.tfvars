tags = {
    XyzApplicationName = "demo"
    XyzApplicationEnvironment = "dev"
    XyzApplicationID = "demo-trfm-1"
    XyzCostCenter = "demo"
    XyzCreatedBy  = "DemoDevOps"
    XyzOnCallContact = "demo-devops@xyz.com"
    XyzCustomerContact = "customer-vip@abcdef.com"
    XyzIaCType = "Terraform"
}

region ="us-west-2"

table_class="STANDARD_INFREQUENT_ACCESS"
