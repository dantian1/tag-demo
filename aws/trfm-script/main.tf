provider "aws" {
  region = local.region
  profile = "demo"

  # Make it faster by skipping something
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

locals {
  bucket_name = "${local.app_env}-${local.app_name}-trfm-bucket-${random_pet.this.id}"
  dynamo_table_name = "${local.app_env}-${local.app_name}-trfm-table-${random_pet.this.id}"

  region      = var.region
  #root_arn    = join("", "arn:aws:iam::", data.aws_caller_identity.current.account_id, ":root"])
  root_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
  owner = lookup(var.tags, "XyzCreatedBy", "Unknown")
  app_name = lookup(var.tags, "XyzApplicationName", "s3-bucket")
  app_env = lookup(var.tags, "XyzApplicationEnvironment", "dev")
}

resource "random_pet" "this" {
  length = 2
}


data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [local.root_arn]
    }

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::${local.bucket_name}",
    ]
  }
}


### Get AWS account ID
### Ref: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity

data "aws_caller_identity" "current" {}

### Created with reference to https://github.com/terraform-aws-modules/terraform-aws-s3-bucket/blob/master/examples/complete/main.tf

module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 3.8"

  bucket              = local.bucket_name
  acl                 = "private"
  force_destroy       = true
  acceleration_status = "Suspended"

  attach_policy = true
  policy        = data.aws_iam_policy_document.bucket_policy.json

  attach_deny_insecure_transport_policy = true
  attach_require_latest_tls_policy      = true

  tags = merge ( 
    var.tags, 
    {
      Name  = local.bucket_name
      Owner = local.owner
      XyzConfidentiality = "private-nonconfidential"
    }
  )
  versioning = {
    enabled = false
  }


  # S3 bucket-level Public Access Block configuration
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  # S3 Bucket Ownership Controls
  control_object_ownership = true
  object_ownership         = "BucketOwnerPreferred"
}

### Created with reference to https://github.com/terraform-aws-modules/terraform-aws-dynamodb-table/blob/master/examples/basic/main.tf 

module "dynamodb_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"
  version = "~> 3.1"

  name        = local.dynamo_table_name
  hash_key    = "LockID"
  table_class = var.table_class

  attributes = [
    {
      name = "LockID"
      type = "S"
    }
  ]

  tags = merge( 
    var.tags, 
    {
      Name  = local.dynamo_table_name
      Owner = local.owner
      XyzConfidentiality = "private-nonconfidential"
    }
  )
}


