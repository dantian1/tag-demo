variable "tags" {
  description = "a map of tags to add to all resources"
  type        = map(string)
  default     = {
    XyzApplicationName = "demo"
    XyzApplicationEnvironment = "dev"
    XyzApplicationID = "demo-trfm-1"
    XyzCostCenter = "demo"
    XyzCreatedBy  = "DemoDevOps"
    XyzOnCallContact = "demo-devops@xyz.com"
    XyzCustomerContact = "customer-vip@abcdef.com"
    XyzIaCType = "Terraform"
  }
}


variable "region" {
  description = "The name of the AWS region that contains the created resources, e.g., us-east-1, us-west-2"
  type = string
  default = "us-west-2"
  validation {
    condition = contains(["us-east-1", "us-east-2", "us-west-1", "us-west-2"], var.region)
    error_message = "Region must be a valid AWS region name."
  }
}

variable "table_class" {
  description = "The storage class of the table. Valid values are STANDARD and STANDARD_INFREQUENT_ACCESS"
  type        = string
  default     = "STANDARD"
}


