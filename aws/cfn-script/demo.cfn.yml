AWSTemplateFormatVersion: "2010-09-09"
Description: This template creates a S3 bucket and a Dynamo DB table for terraform state management
## Ref: https://www.terraform.io/language/settings/backends/s3
## Ref: https://quileswest.medium.com/how-to-lock-terraform-state-with-s3-bucket-in-dynamodb-3ba7c4e637
## Ref: https://jhooq.com/terraform-state-file-locking/

Parameters:
  Environment:
    Type: String
    Description: The deployment environment for the created AWS resources, e.g., test, dev, qa, prod
    Default: dev

  AppName:
    Type: String
    Description: The name of the applicaiton that uses the created AWS resources.
    Default: demo

Resources:
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${Environment}-${AppName}-cfn-bucket
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true
      Tags:
        - Key: Name
          Value: !Sub ${Environment}-${AppName}-cfn-bucket
        - Key: Owner
          Value: "DemoDevOps"
        - Key: 'XyzConfidentiality'
          Value: 'private-nonconfidential'
       
  S3BucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      PolicyDocument:
        Version: 2012-10-17
        Id: !Sub ${Environment}-${AppName}-cfn-bucket-S3BucketPolicy
        Statement:
          - Sid: Allow-all-access-for-users-in-same-account
            Effect: Allow
            Principal:
              AWS:
                - !Sub arn:aws:iam::${AWS::AccountId}:root
            Action:
              - s3:*
            Resource: !Sub arn:aws:s3:::${Environment}-${AppName}-cfn-bucket/*
      Bucket: !Ref S3Bucket

  DdbTable:
    Type: AWS::DynamoDB::Table
    Properties:
      TableName: !Sub ${Environment}-${AppName}-cfn-table
      TableClass: STANDARD_INFREQUENT_ACCESS
      AttributeDefinitions:
        - 
          AttributeName: LockID
          AttributeType: S
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        -
          AttributeName: LockID
          KeyType: HASH
      Tags:
        - Key: Name
          Value: !Sub ${Environment}-${AppName}-cfn-bucket
        - Key: Owner
          Value: "DemoDevOps"
        - Key: 'XyzConfidentiality'
          Value: 'private-nonconfidential'

Outputs:
  S3BucketARN:
    Description: ARN of the s3 bucket
    Value: !GetAtt S3Bucket.Arn

  DdbTableARN:
    Description: ARN of the dynamo db table created with this template
    Value: !GetAtt DdbTable.Arn

  DdbTableName:
    Description: Name of the dynamo db table created with this template
    Value: !Ref DdbTable

