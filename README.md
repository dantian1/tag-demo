# Tagging for Cloud Computing Resource in AWS
 
Tagging is one of the best practices in creating cloud computing resources.
It involves creating and storing multiple key-value pairs of strings that describe
the created resources. Many 
[DevSecOps](https://www.ibm.com/topics/devsecops) and 
[FinOps](https://www.finops.org/introduction/what-is-finops/) practices 
rely on tagging.
The [AWS tagging guide](https://docs.aws.amazon.com/general/latest/gr/aws_tagging.html) 
contains more recommendation, categorization and best practices for tagging.

This ariticle uses samples to demonstrate tagging implementation using popular Infrastructure As Code (IaC)
tools, namely Terraform and CloudFormation. 
Terraform, created by HashiCorp, supports multiple public cloud, such as AWS, Azure, and GCP. 
CloudFormation, created by Amazon, supports only AWS.
We illustrate that both Terraform and CloudFormation are able to meet 
the same business requirements on tagging. This helps to promote best 
practices for tagging in IaC implementation. It also helps users of 
Terraform and CloudFormation to implement tagging effectively. 

In the rest of this document, we first describe sample tags.
We then show scripts for creating the sample tags for cloud computing resources. 
Finally, we provide some summary discussions.


## Sample Tags

Our selection of sample tags and their recommended values are based on the 
[AWS tagging guide](https://docs.aws.amazon.com/general/latest/gr/aws_tagging.html). These sample
tags are for demonstration purposes only.
We will have scripts to create cloud computing resources in AWS with these sample tags. 

**ApplicationEnvironment**: Distinguish between development, test, and production resources. It usually maps to the name of the deployment environment. Allowed values include dev, snbx (sandbox), stgn (staging), test, prod (production). 

**ApplicationID**: Identify resources that are related to a specific application. Allowed value is a string that uniqly identifies an application (e.g., a GitHub repo name, an team id in the Change Control Management system, etc.) within an organization. 

**ApplicationName**: The value for this tag is a more intuitive application (or product) name (e.g., ShoppingCart, UserComments, LeaderBoard, Demo, PoC) in an organization.

**Confidentiality**: Applicable to data storage resources. Values could be public, private-nonconfidential, private-confidential.

**CostCenter**: The value for this tag is a string identifying a unit in an organization for cost assignment on the created resources.

**CreatedBy**: The value for this tag is a string identifying a person or a team responsible for creating the underlying resources.

**IaCType**: This tag indicates the Infrastructure As Code (IaC) language type. Values could be Terraform, CloudFormation, Bash, Python, etc. 

**SupportContact**: The contact information for a resource. If necessary, replace this tag with tags **OnCallContact** or **CustomerContact** (or both).

Please note the above sample tags are for illustration purposes. 
They are not exhausitive for an organization. 
An organization could support more (or less) tags based on business needs. 
It is best for an organization to apply a consistent collection of tags to all its
cloud computing resources.

In our demonstration, we add Xyz as prefix to these sample tag names. 
An organization can replace this prefix with the abbreviation or prefix of the organization's name.

## Sample Cloudformation scripts

The scripts for demonstrating the creation of an AWS S3 bucket and an AWS DynamoDb table 
with the sample tags using CloudFormation are in the [cfn-script](./aws/cfn-script) directory. 
The demo creates the resources in a CloudFormation stack with 
[stack level tags](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-resource-tags.html).


### Create the resources using CloudFormation

Change to the directory cfn-script, and run the following command to create the resources with a CloudFormation stack.

```
% aws cloudformation --profile demo create-stack --template-body file://demo.cfn.yml --cli-input-json file://deploy-params/demo.params.json
{
    "StackId": "arn:aws:cloudformation:us-west-2:012345678901:stack/demo-cfn-tags-dev/8ef45270-c749-11ed-ac6d-0aba4ec10c13"
}
```

### Display information on s3 bucket created by the script

Use AWS CLI command to display information of the created S3 bucket.

```
% aws s3 --profile demo ls         
2023-03-20 11:03:52 dev-demo-cfn-bucket
```

Display the tags for the s3 bucket.

```
% aws s3api --profile demo get-bucket-tagging --bucket dev-demo-cfn-bucket --output text|sort
TAGSET	Name	dev-demo-cfn-bucket
TAGSET	Owner	DemoDevOps
TAGSET	XyzApplicationEnvironment	dev
TAGSET	XyzApplicationID	demo-cfn-1
TAGSET	XyzApplicationName	demo
TAGSET	XyzConfidentiality	private-nonconfidential
TAGSET	XyzCostCenter	demo
TAGSET	XyzCreatedBy	DemoDevOps
TAGSET	XyzCustomerContact	customer-vip@abcdef.com
TAGSET	XyzIaCType	CouldFormation
TAGSET	XyzOnCallContact	demo-devops@xyz.com
TAGSET	aws:cloudformation:logical-id	S3Bucket
TAGSET	aws:cloudformation:stack-id	arn:aws:cloudformation:us-west-2:012345678901:stack/demo-cfn-tags-dev/8ef45270-c749-11ed-ac6d-0aba4ec10c13
TAGSET	aws:cloudformation:stack-name	demo-cfn-tags-dev
```

Display public access configuration for the s3 bucket

```
% aws s3api --profile demo get-public-access-block --bucket  dev-demo-cfn-bucket
{
    "PublicAccessBlockConfiguration": {
        "BlockPublicAcls": true,
        "IgnorePublicAcls": true,
        "BlockPublicPolicy": true,
        "RestrictPublicBuckets": true
    }
}
```

Display the s3 bucket access policies.

```
% aws s3api --profile demo get-bucket-policy --bucket dev-demo-cfn-bucket --output text|jq .
{
  "Version": "2012-10-17",
  "Id": "dev-demo-cfn-bucket-S3BucketPolicy",
  "Statement": [
    {
      "Sid": "Allow-all-access-for-users-in-same-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:root"
      },
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::dev-demo-cfn-bucket/*"
    }
  ]
}
```

### Display information on DynamoDb table created by the script

Display the DynamoDb table created.

```
% aws dynamodb --profile demo list-tables --output text
TABLENAMES      dev-demo-cfn-table
```

Show information about the DynamoDb table.

```
% aws dynamodb --profile demo describe-table --table-name  dev-demo-cfn-table
{
    "Table": {
        "AttributeDefinitions": [
            {
                "AttributeName": "LockID",
                "AttributeType": "S"
            }
        ],
        "TableName": "dev-demo-cfn-table",
        "KeySchema": [
            {
                "AttributeName": "LockID",
                "KeyType": "HASH"
            }
        ],
        "TableStatus": "ACTIVE",
        "CreationDateTime": "2023-03-20T11:03:51.320000-07:00",
        "ProvisionedThroughput": {
            "NumberOfDecreasesToday": 0,
            "ReadCapacityUnits": 0,
            "WriteCapacityUnits": 0
        },
        "TableSizeBytes": 0,
        "ItemCount": 0,
        "TableArn": "arn:aws:dynamodb:us-west-2:012345678901:table/dev-demo-cfn-table",
        "TableId": "e7138a6f-4778-4c79-ad2d-2cbd14f2d329",
        "BillingModeSummary": {
            "BillingMode": "PAY_PER_REQUEST",
            "LastUpdateToPayPerRequestDateTime": "2023-03-20T11:03:51.320000-07:00"
        }
    }
}
```

Display tags for the DynamoDb table.

```
% aws dynamodb --profile demo list-tags-of-resource --resource-arn "arn:aws:dynamodb:us-west-2:012345678901:table/dev-demo-cfn-table" --output text |sort
TAGS	Name	dev-demo-cfn-bucket
TAGS	Owner	DemoDevOps
TAGS	XyzApplicationEnvironment	dev
TAGS	XyzApplicationID	demo-cfn-1
TAGS	XyzApplicationName	demo
TAGS	XyzConfidentiality	private-nonconfidential
TAGS	XyzCostCenter	demo
TAGS	XyzCreatedBy	DemoDevOps
TAGS	XyzCustomerContact	customer-vip@abcdef.com
TAGS	XyzIaCType	CouldFormation
TAGS	XyzOnCallContact	demo-devops@xyz.com
```

### Clean up the created resources 

Run the CloudFormation command to remove the stack.

```
% aws cloudformation --profile demo delete-stack --stack-name demo-cfn-tags-dev
```

Please note [Cloudformation only supports limited tag propagation](https://aws.amazon.com/premiumsupport/knowledge-center/cloudformation-propagate-stack-level-tag/). 

## Sample Terraform scripts

The scripts for demonstrating the creation of a S3 bucket and a DynamoDb table with the 
sample tags using Terraform are in the [trfm-script](./aws/trfm-script) directory. 
Our scripts [merge default tags](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/guides/resource-tagging#getting-started-with-resource-tags) defined in a variable to specific tags for a resource.


### Create the resources using Terraform

Change into the trfm-script directory, and run the following commands to create the resources.

```
% terraform init -reconfigure
% terraform plan -var-file demo.tfvars
% terraform apply -var-file demo.tfvars
```

### Display information on s3 bucket created by the script

Use AWS CLI command to display information of the created S3 bucket.

```
% aws s3 --profile demo ls
2023-03-20 11:31:02 dev-demo-trfm-bucket-concise-bluegill
```

Show tags for the s3 bucket.

```
% aws s3api --profile demo get-bucket-tagging --bucket dev-demo-trfm-bucket-concise-bluegill --output text|sort
TAGSET	Name	dev-demo-trfm-bucket-concise-bluegill
TAGSET	Owner	DemoDevOps
TAGSET	XyzApplicationEnvironment	dev
TAGSET	XyzApplicationID	demo-trfm-1
TAGSET	XyzApplicationName	demo
TAGSET	XyzConfidentiality	private-nonconfidential
TAGSET	XyzCostCenter	demo
TAGSET	XyzCreatedBy	DemoDevOps
TAGSET	XyzCustomerContact	customer-vip@abcdef.com
TAGSET	XyzIaCType	Terraform
TAGSET	XyzOnCallContact	demo-devops@xyz.com
```

Show access configuration for the s3 bucket.

```
% aws s3api --profile demo get-public-access-block --bucket  dev-demo-trfm-bucket-concise-bluegill
{
    "PublicAccessBlockConfiguration": {
        "BlockPublicAcls": true,
        "IgnorePublicAcls": true,
        "BlockPublicPolicy": true,
        "RestrictPublicBuckets": true
    }
}
```

Show access policies for the s3 bucket.

```
% aws s3api --profile demo get-bucket-policy --bucket dev-demo-trfm-bucket-concise-bluegill --output text|jq .
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "denyOutdatedTLS",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::dev-demo-trfm-bucket-concise-bluegill/*",
        "arn:aws:s3:::dev-demo-trfm-bucket-concise-bluegill"
      ],
      "Condition": {
        "NumericLessThan": {
          "s3:TlsVersion": "1.2"
        }
      }
    },
    {
      "Sid": "denyInsecureTransport",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::dev-demo-trfm-bucket-concise-bluegill/*",
        "arn:aws:s3:::dev-demo-trfm-bucket-concise-bluegill"
      ],
      "Condition": {
        "Bool": {
          "aws:SecureTransport": "false"
        }
      }
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:root"
      },
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::dev-demo-trfm-bucket-concise-bluegill"
    }
  ]
}
```

### Show information on the DynamoDb table created

Show the DynamoDb table.

```
% aws dynamodb --profile demo list-tables --output text
TABLENAMES      dev-demo-trfm-table-concise-bluegill
```

Show descriptions of the DynamoDb table.

```
% aws dynamodb --profile demo describe-table --table-name dev-demo-trfm-table-concise-bluegill
{
    "Table": {
        "AttributeDefinitions": [
            {
                "AttributeName": "LockID",
                "AttributeType": "S"
            }
        ],
        "TableName": "dev-demo-trfm-table-concise-bluegill",
        "KeySchema": [
            {
                "AttributeName": "LockID",
                "KeyType": "HASH"
            }
        ],
        "TableStatus": "ACTIVE",
        "CreationDateTime": "2023-03-20T11:30:57.612000-07:00",
        "ProvisionedThroughput": {
            "NumberOfDecreasesToday": 0,
            "ReadCapacityUnits": 0,
            "WriteCapacityUnits": 0
        },
        "TableSizeBytes": 0,
        "ItemCount": 0,
        "TableArn": "arn:aws:dynamodb:us-west-2:012345678901:table/dev-demo-trfm-table-concise-bluegill",
        "TableId": "2455622b-b2ca-4b5c-8896-30638e381dad",
        "BillingModeSummary": {
            "BillingMode": "PAY_PER_REQUEST",
            "LastUpdateToPayPerRequestDateTime": "2023-03-20T11:30:57.612000-07:00"
        }
    }
}
```

Show tags for the DynamoDb table.

```
% aws dynamodb --profile demo list-tags-of-resource --resource-arn "arn:aws:dynamodb:us-west-2:012345678901:table/dev-demo-trfm-table-concise-bluegill" --output text |sort
TAGS	Name	dev-demo-trfm-table-concise-bluegill
TAGS	Owner	DemoDevOps
TAGS	XyzApplicationEnvironment	dev
TAGS	XyzApplicationID	demo-trfm-1
TAGS	XyzApplicationName	demo
TAGS	XyzConfidentiality	private-nonconfidential
TAGS	XyzCostCenter	demo
TAGS	XyzCreatedBy	DemoDevOps
TAGS	XyzCustomerContact	customer-vip@abcdef.com
TAGS	XyzIaCType	Terraform
TAGS	XyzOnCallContact	demo-devops@xyz.com
```

### Delete the resources created using Terraform

Run the following command to delete the created resources.

```
% terraform destroy
```

## Conclusion

Our implementation of the sample tags provides a template for tagging AWS resources consistently.  
Further enhancement of the implementation can add validation of the parameter (or variable) values
to the scripts.

It is possible to use the S3 bucket and the DynamoDb table created in the sample scripts to support configuration for
[Terraform backend](https://developer.hashicorp.com/terraform/language/settings/backends/s3). In such use cases, please update
the scripts by adding a configuration setting for the S3 bucket to support bucket version.

To support identification of scripts used for creating the resources, consider using additional tags such as IaCScriptUrl to store 
the code repository location information for the IaC scripts.




